<?php
	
	define('USD', 'usd');
	define('EUR', 'eur');
	define('UAH', 'uah');

	define('TYPE_PERCENT', 'percent');
	define('TYPE_FIXED', 'fixed');

	define(
		'RATES', 
		[
			'uah' => [
				'name' => 'Гривна',
				'course' => 1,
			],
			'usd' => [
				'name' => 'Доллар',
				'course' => 27.1,
			],
			'eur' => [
				'name' => 'Евро',
				'course' => 30.2,
			],
		]
	);

	require_once 'array.php'; 
	require_once 'functions.php';
	
?>
<!DOCTYPE html>
<html>
<head>
	<title>Products List</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<body>
	<div class="container">
		<h1 align="center">Список продукции</h1>
		<table class="table">
			<tr>
				<th>Наименование</th>
				<th>Цена</th>
				<th>Валюта</th>
				<th>Скидка</th>
				<th>Цена со скидкой</th>
			</tr>
			<?php foreach ($products as $product) : ?>
				<?php $price = toUah($product['price'], $product['currency']) ?>
				<?php $discount = makeDiscount($price, $product['discountType'], $product['discountValue'], $product['currency']) ?>
				<tr>
					<td><?= $product['title'] ?></td>
					<td><?= $price ?></td>
					<td>грн</td>
					<td><?= $discount ?></td>
					<td><?= $price - $discount ?></td>
				</tr>
			<?php endforeach ?>
		</table>
	</div>
</body>
</html>