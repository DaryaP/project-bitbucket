<?php

function toUah($amount, $currency)
{
	return $amount * RATES[$currency]['course'];
}

function makeDiscount($amount, $type, $value, $currency)
{
	switch ($type) {
		case TYPE_PERCENT:
			return $amount / 100 * $value;
			break;

		case TYPE_FIXED:
			return $value;//скидка указывается в гривне вне зависимости от валюты, в которой указана цена
			break;
	}
}